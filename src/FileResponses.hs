{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
module FileResponses
  ( getResource
  )
where

import           Text.Printf                    ( printf )
import           Text.RawString.QQ
import qualified Data.ByteString.Lazy          as LS
import           System.Directory
import           System.FilePath
import           Control.Exception
import           Utils.StringUtils              ( stripLeading )

import           Data.List                      ( isPrefixOf )
import           Data.Typeable                  ( typeOf )

import           HttpContentType                ( HttpContentType(..)
                                                , getContentType
                                                )

getResource :: Maybe String -> IO (LS.ByteString, HttpContentType)
getResource resourceMb = do
  dir <- getCurrentDirectory
  let assetsDir   = dir </> "assets"
  let fileToServe = getFileToServe resourceMb assetsDir
  printf "File to serve: %s\n" fileToServe
  readFileOrGetDefaultHtml fileToServe

-- These are our routes: Map ressources which the clients
-- requests to the files the server sends to the client
getFileToServe :: Maybe String -> FilePath -> FilePath
getFileToServe Nothing                  assetsDir = assetsDir </> indexHtml
getFileToServe (Just requestedResource) assetsDir = case requestedResource of
  "/"        -> assetsDir </> indexHtml
  "/img.svg" -> assetsDir </> "images" </> "bull_bytes.svg"
  "/text"    -> plainTextDir </> "hello"
  -- We remove the slash since pathname's </> combines
  -- "dir" </> "/file" to just "/file" and not "dir/file"
  _ ->
    let fileToRead = assetsDir </> stripLeading '/' requestedResource
            --  Make sure only files inside the assets dir are served.
    in  if isInside assetsDir fileToRead
          then fileToRead
          else plainTextDir </> "access_forbidden"
  where plainTextDir = assetsDir </> "plaintext"

-- Checks whether a file is inside a directory or its subdirectories
isInside :: FilePath -> FilePath -> Bool
-- We add a trailing slash to the dir so "/home/me/dir" isn't interpreted
-- as containing "/home/me/dir2"
isInside dir file = addTrailingPathSeparator dir `isPrefixOf` file

indexHtml = "html" </> "index.html"

readFileOrGetDefaultHtml :: FilePath -> IO (LS.ByteString, HttpContentType)
readFileOrGetDefaultHtml filePath = do
  readResult <-
    (try $ LS.readFile filePath) :: IO (Either IOException LS.ByteString)
  case readResult of
    Left ex -> do
      printf "Exception of type %s: %s\n" (show (typeOf ex)) (show ex)
      putStrLn "Returning default HTML"
      return (LS.fromStrict defaultHtml, HttpContentType.Html)
    Right fileContent -> return (fileContent, getContentType filePath)
 where
  defaultHtml = [r|
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <title>Bull Bytes</title>
  </head>
  <body>
    <h2>Default text</h2>
  </body>
</html>
|]
