module HttpContentType where

import           System.FilePath
import           Utils.StringUtils              ( toLowerCase )

data HttpContentType =
   PlainText
 | Html
 | Css
 | Svg
 | Png
 | Gif
 | Jpeg


-- See https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
instance Show HttpContentType where
  show PlainText = "text/plain"
  show Html      = "text/html"
  show Css       = "text/css"
  show Svg       = "image/svg+xml"
  show Png       = "image/png"
  show Gif       = "image/gif"
  show Jpeg      = "image/jpeg"

getContentType :: FilePath -> HttpContentType
getContentType resource = case lowerCaseExtension of
  ".html" -> Html
  ".css"  -> Css
  ".svg"  -> Svg
  ".png"  -> Png
  ".gif"  -> Gif
  ".jpeg" -> Jpeg
  ".jpg"  -> Jpeg
  _       -> PlainText
  where lowerCaseExtension = toLowerCase $ takeExtension resource
