{-# LANGUAGE OverloadedStrings #-}
module HttpsServer
  ( handleConnection
  )
where
import qualified Network.TLS                   as TLS
import           Data.ByteString.UTF8          as BSU
                                                ( fromString )
import qualified FileResponses                  ( getResource )

import qualified Data.ByteString.Lazy          as LB

import           Control.Exception              ( try )
import           Text.Printf                    ( printf )
import           Data.Default.Class
import           Network.Socket

import qualified Data.ByteString               as BS

import           HttpStatus                    as Status
import qualified HttpHeaders
import qualified Data.ByteString.Lazy.UTF8     as LBU
import qualified Network.TLS.Extra             as TE

import           System.FilePath

handleConnection :: Socket -> IO ()
handleConnection connSock = do
  ctx <- createTlsContext connSock
  sendEncryptedReply ctx

readMsg :: TLS.Context -> IO BS.ByteString
readMsg context = go context mempty
 where
  go context prevContent = do
    -- Read 16 KiB a time
    newContent <- TLS.recvData context
    let content = prevContent <> newContent
    -- Read the data from the socket unless it's empty which means
    -- that the client has closed its socket. We also stop reading
    -- when we see an empty line which marks the end of the HTTP header
    if BS.null newContent || emptyLine `BS.isSuffixOf` content
      then return content
      else go context content
    where emptyLine = BSU.fromString "\r\n\r\n"

sendEncryptedReply :: TLS.Context -> IO ()
sendEncryptedReply ctx = do
  replyWithResource ctx
  TLS.contextClose ctx

replyWithResource tlsContext = do
  clientMsg <- readMsg tlsContext
  let requestedResource = HttpHeaders.requestedResource clientMsg
  (responseBody, contentType) <- FileResponses.getResource requestedResource
  -- To get the number of bytes in the response body, we need ByteString.Lazy's length.
  -- If we used ByteString.Lazy.UTF8's length, we'd get the number of characters which
  -- would lead to a "Content-Length" value in the response that's too short if the
  -- response contains characters that are encoded using more than one byte.
  let headerLines = [HttpHeaders.contentLength (LB.length responseBody)]
  let response =
        LBU.fromString (HttpHeaders.mkHeader Status.ok contentType headerLines)
          <> "\r\n\r\n"
          <> responseBody

  -- https://hackage.haskell.org/package/tls-1.4.1/docs/Network-TLS.html#v:sendData
  TLS.sendData tlsContext response
  TLS.contextFlush tlsContext

createTlsContext :: Socket -> IO TLS.Context
createTlsContext sock = do
  cred <- loadCredentials
  let creds = case cred of
        Right c -> TLS.Credentials [c]
        Left  e -> error e
  ctx <- TLS.contextNew sock (params creds)
  res <- try $ TLS.handshake ctx :: IO (Either TLS.TLSException ())
  printf "Result of handshake: %s\n" (show res)
  return ctx
 where
  params :: TLS.Credentials -> TLS.ServerParams
  params creds = def
    { TLS.serverWantClientCert = False
    , TLS.serverShared = def { TLS.sharedCredentials = creds }
    , TLS.serverSupported = def { TLS.supportedVersions = [TLS.TLS12]
                                , TLS.supportedCiphers = [TE.cipher_AES256_SHA1]
                                }
    }

loadCredentials :: IO (Either String TLS.Credential)
loadCredentials = do
  printf "Loading TLS credentials from directory %s\n" certDir
  -- https://hackage.haskell.org/package/tls-1.5.1/docs/Network-TLS.html#v:credentialLoadX509
  TLS.credentialLoadX509 (certDir </> "server.crt") (certDir </> "server.key")
  where certDir = "cert"
