-- This module starts either a HTTPS, HTTP, or TCP server, depending on the passed command line arguments.
-- See the README file for more information.
module Main where
import           System.Environment             ( getArgs )
import           Text.Printf                    ( printf )
import           Data.List                      ( elemIndex )
import           Safe                           ( atMay )
import           Text.Read                      ( readMaybe )
import           Data.Maybe                     ( fromMaybe )
import           Utils.StringUtils              ( toLowerCase )


import qualified TcpServer
import qualified HttpsServer
import qualified HttpServer

data ServerMode = Tcp
                | Https
                | Http
                deriving (Show)

-- Starts the server on port 8080.
-- Provide a custom port via the command line: "--port 9090"
-- In GHCi, you can set command line arguments with :set args "--port" "8085"
main :: IO ()
main = do
  cmdLineArgs <- getArgs
  printf "%d command line arguments: %s\n" (length cmdLineArgs)
    $ unwords cmdLineArgs
  let portNr     = getPortNr cmdLineArgs
  let serverMode = getServerMode cmdLineArgs
  printf "Starting %s server\n" $ show serverMode
  TcpServer.runServer portNr $ case serverMode of
    Https -> HttpsServer.handleConnection
    Http  -> HttpServer.handleConnection
    Tcp   -> TcpServer.echoMsg

getServerMode :: [String] -> ServerMode
getServerMode cmdLineArgs = fromMaybe defaultMode modeMaybe
 where
  defaultMode = Https
  modeMaybe   = do
    -- We expect the server mode after this flag
    modeStr <- getElemAfter cmdLineArgs "--mode"
    return $ case toLowerCase modeStr of
      "http" -> Http
      "tcp"  -> Tcp
      _      -> Https

getElemAfter :: [String] -> String -> Maybe String
getElemAfter list elem = do
  idx <- elemIndex elem list
  atMay list (1 + idx)

getPortNr :: [String] -> Int
getPortNr cmdLineArgs = fromMaybe defaultPort portMaybe
 where
  defaultPort = 8080
  portMaybe   = do
    -- We expect the port number after this flag
    portStr <- getElemAfter cmdLineArgs "--port"
    readMaybe portStr
