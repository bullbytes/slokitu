module HttpServer
  ( handleConnection
  )
where
import           Network.Socket          hiding ( recv )
import           Network.Socket.ByteString      ( recv
                                                , sendAll
                                                )

import qualified Data.ByteString               as BS
import           Data.ByteString.UTF8          as BSU
                                                ( fromString )

import           HttpHeaders
import           HttpStatus                    as Status


import           HttpContentType                ( HttpContentType(..) )

readHeader :: Socket -> IO BS.ByteString
readHeader sock = go sock mempty
 where
  go sock prevContent = do
    newContent <- recv sock maxNumberOfBytesToReceive
    let content = prevContent <> newContent
    -- Read the data from the socket unless it's empty which means
    -- that the client has closed its socket. We also stop reading
    -- when we see an empty line which marks the end of the HTTP header
    if BS.null newContent || emptyLine `BS.isSuffixOf` content
      then return content
      else go sock content
   where
    -- Only read one byte at a time to avoid reading further than the
    -- empty line separating the HTTP header from the HTTP body
    maxNumberOfBytesToReceive = 1
    emptyLine                 = BSU.fromString "\r\n\r\n"

handleConnection :: Socket -> IO ()
handleConnection connSock = do
  clientHeader <- readHeader connSock
  let resourceMaybe = requestedResource clientHeader
  let
    responseBody =
      BSU.fromString
          (  "The 💁 responds:\r\nRequested resource: "
          <> show resourceMaybe
          <> "\r\n"
          )
        <> BSU.fromString "Your header:\r\n"
        <> clientHeader
        <> BSU.fromString "End of server response\r\n"
  let headerLines = [contentLength (BS.length responseBody), plainTextInUtf8]
  let responseHeader =
        BSU.fromString
          $  mkHeader Status.ok HttpContentType.PlainText headerLines
          <> "\r\n\r\n"
  sendAll connSock $ responseHeader <> responseBody
