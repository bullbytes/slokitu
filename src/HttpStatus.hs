module HttpStatus
  ( HttpStatus
  , ok
  , notFound
  )
where

data HttpStatus = HttpStatus Int String

-- See https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
ok = HttpStatus 200 "OK"
notFound = HttpStatus 404 "Not Found"

instance Show HttpStatus where
  show (HttpStatus code message) = show code <> " " <> message
