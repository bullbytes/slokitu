# Slokitu
A minimal web server built on sockets.

Start it with `stack ghci` in the project's root directory and then run `main`.

Send the server an HTTPS request with `curl -k https://localhost:8080/` to get an HTML response.

The server can also serve an image:

    curl -Ok https://localhost:8080/img.svg

as well as plain text:

    curl -Ok https://localhost:8080/text


To change the port the server listens to, use `:set args "--port" "8085"` in GHCi.

## Server modes
The `--mode` command line flag allows for different server modes:

  * HTTPS (default)
  * HTTP
  * TCP

### TCP echo server
After starting the TPC server by setting the command line parameter in GHCi with `:set args "--mode" "tcp"`, start a [Telnet](https://en.wikipedia.org/wiki/Telnet) session using

    telnet localhost 8080

Type a message, hit enter and the server will reply to you.

