module TcpServer
  ( runServer
  , echoMsg
  )
where
-- https://hackage.haskell.org/package/network-2.8.0.1/docs/Network-Socket.html
-- See Michael Kerrisk's "The Linux Programming Interface" for more on sockets.
import           Network.Socket          hiding ( recv )
import           Text.Printf                    ( printf )

import           Control.Concurrent             ( forkFinally )
-- https://hackage.haskell.org/package/base-4.12.0.0/docs/Control-Exception.html#v:bracket
import qualified Control.Exception             as E
                                                ( bracket )

import           Control.Monad                  ( forever
                                                , unless
                                                )
import           Data.ByteString.UTF8          as BSU
                                                ( fromString )
import qualified Data.ByteString               as S
import           Network.Socket.ByteString      ( recv
                                                , sendAll
                                                )

runServer :: Int -> (Socket -> IO ()) -> IO ()
runServer portNr handleConnection = withSocketsDo $ E.bracket
  -- Acquire the socket
  (getListeningSocket portNr)
  -- If the server loop is interrupted (with SIGKILL, for example), we close the socket
  (\serverSock -> do
    close serverSock
    printf "\nClosed listening socket on port %d\n" portNr
  )
  -- The server loop: accept connections from the listening socket and handle connections
  loop
 where
  loop :: Socket -> IO ()
  loop serverSock = forever $ do
    (connSock, peerAddress) <- accept serverSock
    printf "Accepted connection with %s\n" (show peerAddress)
    -- Forks a thread to handle the peer connection. When the
    -- thread is about to terminate, close the socket
    forkFinally
      (handleConnection connSock)

      (\exceptionEither ->
        let exceptionMsg = case exceptionEither of
              Left  exception -> ". Exception: " ++ show exception
              Right ()        -> ""
        in  do
              close connSock
              printf "Closed connection with %s%s\n"
                     (show peerAddress)
                     exceptionMsg
      )

getListeningSocket :: Int -> IO Socket
getListeningSocket portNr = do
  addrInfo <- getSocketAddress portNr
  sock     <- socket (addrFamily addrInfo)
                     (addrSocketType addrInfo)
                     (addrProtocol addrInfo)
  configure sock
  bind sock (addrAddress addrInfo)
  listen sock maxNrOfQueuedConnections
  printf "Created listening socket: %s\n" (show addrInfo)
  return sock
  where maxNrOfQueuedConnections = 5

configure :: Socket -> IO ()
configure socket = do
  -- https://stackoverflow.com/questions/6125068/what-does-the-fd-cloexec-fcntl-flag-do
  setCloseOnExecIfNeeded $ fdSocket socket
  -- Setting the SO_REUSEADDR option means that we can bind a socket to a local port
  -- even if another TCP is bound to the same port either because:
  -- 1. A previous invocation of the server that was connected to a client
  -- performed an active close, either by calling close(), or by crashing (e.g., it was
  -- killed by a signal). This leaves a TCP endpoint that remains in the TIME_WAIT
  -- state until the 2MSL timeout expires.
  --
  -- 2. A previous invocation of the server created a child process to handle a connection
  -- to a client. Later, the server terminated, while the child continues to serve
  -- the client, and thus maintain a TCP endpoint using the server's well-known port.
  setSocketOption socket ReuseAddr 1

getSocketAddress :: Int -> IO AddrInfo
    -- getAddrInfo resolves a hostname to one or more addresses (possibly using DNS) returning the best first.
    -- It's multiple addresses if, for example, the host has more than one network interface.
    -- It also contains a socket address created using the hints.
    -- See also https://hackage.haskell.org/package/network-3.1.1.0/docs/Network-Socket.html#v:getAddrInfo
getSocketAddress portNr = head <$> getAddrInfo hints hostAddress serviceName
 where
  hostAddress = Nothing -- ^Passing nothing gets the wildcard address. For IPv6 this is 0::0 and for IPv4 it's 0.0.0.0
  serviceName = Just (show portNr) -- ^Can also be a string like "http"
  -- A passive socket that listens for incoming client connections.
  -- AI stands for address info.
  -- Stream means we want a TCP connection.
  -- AF_INET6 is IPv6
  hints       = Just $ defaultHints { addrFlags      = [AI_PASSIVE]
                                    , addrSocketType = Stream
                                    , addrFamily     = AF_INET6
                                    }

echoMsg :: Socket -> IO ()
echoMsg connSock = do
  msg <- recv connSock maxNumberOfBytesToReceive
  -- Echo the byte string unless it's empty which means that the
  -- client has closed its socket
  unless (S.null msg) $ do
    sendAll connSock (BSU.fromString "💁 reply: " <> msg)
    echoMsg connSock
  where maxNumberOfBytesToReceive = 1024
