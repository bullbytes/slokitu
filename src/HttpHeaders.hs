module HttpHeaders where

import           Data.List                      ( intercalate )
import           HttpStatus
import           HttpContentType                ( HttpContentType(..) )

import qualified Data.ByteString               as BS
                                                ( ByteString )
import           Data.ByteString.UTF8          as BSU
import           Safe                           ( atMay
                                                , headMay
                                                )
import           Data.List.Split                ( splitOn )

contentLengthHeader = "Content-Length: "
contentTypeHeader = "Content-Type: "
httpVersion = "HTTP/1.1"
lineBreak = "\r\n"
utf8 = "charset=UTF-8"
plainTextInUtf8 =
  contentTypeHeader <> show HttpContentType.PlainText <> "; " <> utf8
htmlInUtf8 = contentTypeHeader <> show HttpContentType.Html <> "; " <> utf8

contentLength :: (Show i, Integral i) => i -> String
contentLength length = contentLengthHeader <> show length

mkHeader :: HttpStatus -> HttpContentType -> [String] -> String
mkHeader status contentType headerLines =
  statusLine <> lineBreak <> intercalate lineBreak
                                         (contentTypeLine : headerLines)
 where
  statusLine      = httpVersion <> " " <> show status
  contentTypeLine = contentTypeHeader <> show contentType <> "; " <> utf8


requestedResource :: BS.ByteString -> Maybe String
requestedResource header =
  let lines = splitOn "\r\n" (BSU.toString header)
  in  do
        firstLine <- headMay lines
        let elements = words firstLine
        -- First comes the HTTP method, then the resource, then the HTTP version
        atMay elements 1
